<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;

require_once "vendor/autoload.php";
require_once "Scout.php";

$db = [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'scouts',
    'username'  => 'admin',
    'password'  => 'admin',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];

$capsule = new Capsule;
$capsule->addConnection($db);
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();

function debug($a){
    echo "<pre>";
    var_dump($a);
    echo "</pre>";
}

$server = new \Ddeboer\Imap\Server(
    "imap.yandex.ru",
    "993"
);

$search = new \Ddeboer\Imap\SearchExpression();
$search->addCondition(new \Ddeboer\Imap\Search\Text\Subject("Web Scout"));
$connection = $server->authenticate("scout@wtolk.ru", "webscout12");
$folder = $connection->getMailbox("INBOX");
$messages = $folder->getMessages($search);

foreach ($messages as $message){
    $scout_name = $message->getFrom()->getAddress();
    if(Scout::all()->where("email", $scout_name)->count() == 0) {

        foreach ($message->getAttachments() as $attachment){
            file_put_contents(
                "zips/".$attachment->getFilename(),
                $attachment->getDecodedContent()
            );
            $zippy = \Alchemy\Zippy\Zippy::load();
            $archive = $zippy->open("zips/".$attachment->getFilename());
            $archive->extract("zips");
            foreach ($archive->getMembers() as $member){
                $name = $member->getLocation();
                $archive->removeMembers($name);
            }
            $content = file("zips/".$name);
            unlink("zips/".$name);

            preg_match_all("/\'\H+\'/i", $content[1], $email_array);
            $email = $email_array[0][0];
            $email = substr($email, 1, strlen($email)-2);

            preg_match_all("/\'\H+\'/i", $content[4], $hash_array);
            $hash = $hash_array[0][0];
            $hash = substr($hash, 1, strlen($hash)-2);

            $my_hash = md5($email);

            if ($my_hash == $hash) {
                echo "<p>Тест пройден ";

                $mail = new Message();
                $mail->setFrom("scout@wtolk.ru")
                    ->addTo($scout_name)
                    ->setSubject("Приглашение в отряд Web скаутов")
                    ->setHtmlBody("<h1>Вы прошли тест и всё такое</h1>");
                $mailer = new SmtpMailer([
                    'host' => 'smtp.yandex.ru',
                    'username' => 'scout@wtolk.ru',
                    'password' => 'webscout12',
                    'secure' => 'ssl'
                ]);
                $mailer->send($mail);
            } else {
                echo "<p>Тест не пройден ";

                $mail = new Message();
                $mail->setFrom("scout@wtolk.ru")
                    ->addTo($scout_name)
                    ->setSubject("Ответ от отряда Web скаутов")
                    ->setHtmlBody("<h1>Вы не прошли тест и всё такое</h1>");
                $mailer = new SmtpMailer([
                    'host' => 'smtp.yandex.ru',
                    'username' => 'scout@wtolk.ru',
                    'password' => 'webscout12',
                    'secure' => 'ssl'
                ]);
                $mailer->send($mail);
            }
            echo $scout_name."</p>";
        }

        Scout::create([
            "email" => $scout_name
        ]);

    }
}






