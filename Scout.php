<?php
/**
 * Created by PhpStorm.
 * User: h
 * Date: 20.07.2018
 * Time: 10:21
 */

class Scout extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "scouts";
    public $timestamps = false;
    protected $fillable = [
        "email"
    ];
}